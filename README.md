# devops-less4

1. docker build -t nginx_less1.4_test .

2. docker run -d \
-it -p 80:80 \
--volume $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf \
--volume $(pwd)/index.html:/usr/local/nginx/html/index.html \
nginx_less1.4_test

3. Для проверки внутри контейнера \
3.1 apt update && apt upgrade -y && apt install links -y \
3.2 links 127.0.0.1
