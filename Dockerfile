FROM debian:9 as build
RUN apt update && apt upgrade -y && apt install make wget gcc zlib1g-dev libpcre3-dev -y
RUN wget https://nginx.org/download/nginx-1.0.5.tar.gz && tar zxvf nginx-1.0.5.tar.gz && cd nginx-1.0.5 && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD [ "./nginx", "-g",  "daemon off;" ]
